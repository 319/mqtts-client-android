# HOW TO USE THIS SAMPLE
- This sample apply for 23/9 park bus station 
- Update ssl files in /assets/ssl for other station

#HOW TO GET REALTIME DATA
Have 2 method to get realtime data, following
- Run App and view LogCat to get accept token, you can you this token to get data via our API
    - Example message in LogCat: things/239-ebus-station/clientToken : "mjmFMxqOtjqx04HwwGm6irkRmVNj01S2AvnUquZmWFs="

- You can get realtime data without make request to our API RESTFUL. Realtime data will automatically send to client via topic: things/239-ebus-station/realtime-data